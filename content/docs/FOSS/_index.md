---
title: FOSS EVENTS 
type: docs
prev: docs/
next: docs/adventures 
sidebar:
  open: true
---

The very first FOSS related Event I attended was the [FOSSMEET'23](https://www.fossmeet.net/) Held at our
campus. From then I got the opurtuinity to attend many other FOSS Events ranging
from monthly FOSS United Kochi meetups to The flagship event of debian
DebConf'23 that was held in Kochi. A slow and steady journal can be expected : ) 
