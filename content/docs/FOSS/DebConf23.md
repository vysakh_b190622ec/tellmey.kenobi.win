---
title: "DebConf 23"
date: 2023-10-06T19:46:17+05:30 
---
![](/Pages/Home/debconf23_group.jpg)
\
I first got to know about DebConf'23 from one of the last place one can know of
it. Debian India's Instagram account had posted a poster that said 252 days to
go! They had posted this on Jan 1st 2023, later in February during the FOSSMEET,
My school time friend Akash mentioned to me about the event and explained how
grand the event was. Without hesitiation I told him to consider my application
when they start the entries. But tbh I never really thought I'd be able to
participate as he had already told me the seats are very few and there are a lot
of Internationals also attending and they had seats reserved.
<br> As soon as the registration portal opened I made account and started the
registration process. In my mind it was another 1 week long FOSS event and since
I'm from kochi my plan was to stay home and go attend the event everyday and
thereby I didn't care to look anywhere near the bursaries(not to mention I had
no idea it meant sponsorship).
It was during the Debutsav event that took place at MEC [Abraham](abrahamraji.in/) told me to apply for the
bursary. I remembered that the bursary was supposed to be there for the debian devolopers and people who 
are working in close with debian. Up until that point that self doubt was preventing me from filling up the bursary request
but Abraham mentioned that the bursary is open for "Potential New Contributors"
which gave me the confidence I needed to apply for the bursary. I was supposed
to fill 3 boxes descriptively answering various questions that I'm supposed to
answer in order to increase the chance of getting bursary.<br>

Within few weeks the DebConf team had approved my bursary request. I didn't waste a second to flex about getting the bursary approved and hype up myself and 
my friends about the event. The fact that the event was held one week before my midsems didn't seem to slow me down from going forward anyways. I've written 
my experience about the [DebConf'23](https://newsletter.fosscell.org/post/debconf23/) over at the [Newsletter](https://newsletter.fosscell.org) website of FOSSCell NITC. 
Do check that out.  
