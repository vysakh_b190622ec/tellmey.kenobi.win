---
title: Life at NITC 
type: docs
prev: docs/
next: docs/adventures 
sidebar:
  open: true
---
 
So I'm currently on my 5th year of a 4 year B.Tech Degree at [NIT Calicut](https://nitc.ac.in/).
I've been part of many clubs and organisations and I'll try to feature a gist of
the events that I got to conuct or participate with them. Chances are I'll be
featuring most of the events directly or Indirectly Related to [FOSSCELL NITC](https://www.fosscell.org/).
Let's get started with [Friday Night FOSS](fridaynightfoss)

